export const hmsToSecondsOnly = function (string) {
    let p = string.split(':'),
        s = 0, m = 1;

    while (p.length > 0) {
        s += m * parseInt(p.pop(), 10);
        m *= 60;
    }

    return (s);
}