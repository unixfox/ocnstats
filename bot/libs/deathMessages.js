import { getHTTPSContent } from "./utils/httpsPromises.js";
import { default as parseDotProperties } from "dot-properties";
import namedRegExp from "named-regexp-groups";

const deathMessagesFileURL = "https://raw.githubusercontent.com/Electroid/PGM/master/src/main/i18n/templates/strings.properties";

let deathMessages;

function onlyDeathMessages(value) {
    if (value[0] && value[0].includes("death.")) {
        return true;
    }
}

getHTTPSContent(deathMessagesFileURL).then(body => {
    const parsedConfigFile = parseDotProperties.parseLines(body);
    const filterParsedConfigFile = parsedConfigFile.filter(onlyDeathMessages);
    deathMessages = filterParsedConfigFile;
}).catch(err => {
    console.error("An error occurred while trying to fetch the deathMessage file from Github:\n" + err);
    throw(err);
});

export const compareDeathMessage = function (message) {
    return new Promise((resolve, reject) => {
        deathMessages.forEach(deathMessage => {
            if (deathMessage[1] && !deathMessage[0].includes("#"))
                if (!deathMessage[1].includes("{3}") && !deathMessage[1].includes("predicted")) {
                    const regex = new namedRegExp(RegExp(deathMessage[1].replace("{0}", "(:<victimName>[\\w\\d_]+)").replace("{1}", "(:<killerName>[\\w\\d_]+)").replace("{2}", "(:<weapon>.+)").replace("{4}", "(:<distanceBlocks>[\\d]+)")));
                    if (regex.exec(message))
                        resolve(regex.exec(message));
                }
        });
    })
};