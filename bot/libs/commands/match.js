import { hmsToSecondsOnly } from "../utils/time.js"

export const matchTime = function (bot) {
    return new Promise((resolve, reject) => {
        bot.chat("/match");
        bot.once('matchcmd', (value) => {
            const dateInSeconds = Math.round(new Date().getTime() / 1000);
            resolve(dateInSeconds - hmsToSecondsOnly(value));
        });
    });
}