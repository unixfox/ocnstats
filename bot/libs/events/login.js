import { matchTime } from "../commands/match.js";

export default function (bot) {
    bot.on("login", () => {
        console.log("login");
        matchTime(bot).then(time => {
            console.log(time);
        });
    });
}