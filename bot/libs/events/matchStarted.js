import { matchTime } from "../commands/match.js";

export default function (bot) {
    bot.on("matchstarted", () => {
        matchTime(bot).then(time => {
            console.log(time);
        });
    });
}