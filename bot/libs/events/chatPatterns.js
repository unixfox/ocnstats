export default function (bot) {
    bot.chatAddPattern(/^Time: ([\d:]+).(?:[\d]+)$/, "matchcmd", "match command");
    bot.chatAddPattern(/^The match has started!$/, "matchstarted", "the match just started");
    bot.chatAddPattern(/^Game over!$/, "tiematch", "tie match");
    bot.chatAddPattern(/^((?:\[[\w]+\] |[\W]|[\W]\[[\w]+\])?([\w\d_]+)|(?: \[[\w] +\] | [\W] | [\W]\[[\w] +\]) ? ([\w\d_] +) and(?: \[[\w] +\] | [\W] | [\W]\[[\w] +\]) ? ([\w\d_] +))(?: wins | win | winners)!$ /, "matchwin", "end of the match");
}