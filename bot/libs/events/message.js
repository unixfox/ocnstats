import { compareDeathMessage } from "../deathMessages.js";
import stripAnsi from "strip-ansi";
import { kill as killSchema } from "../schemas.js";
import dynamoose from "dynamoose";
dynamoose.local();

const Kill = dynamoose.model("Kill", killSchema);

export default function (bot) {
    bot.on("message", (jsonMsg) => {
        const cleanMessage = stripAnsi(jsonMsg.toAnsi());
        compareDeathMessage(cleanMessage)
            .then(messageParsed => {
                const newKill = new Kill({
                    matchTime: 10,
                    victimUUID: bot.players[messageParsed.groups.victimName].uuid,
                    killerUUID: "hello"
                });
                newKill.save()
                    .then(err => console.log(err));
                console.log(messageParsed);
            });
    });
}