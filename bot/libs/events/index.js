import { promises as fs } from "fs";

export default async function (bot) {
    const modules = (await fs.readdir("./libs/events"))
        .filter(filename => filename !== "index.js")
        .map(async filename => (await import("./" + filename)).default(bot));

    return Promise.all(modules);
}