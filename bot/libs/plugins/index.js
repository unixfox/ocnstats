import { promises as fs } from "fs";

export default async function (bot) {
    const modules = (await fs.readdir("./libs/plugins"))
        .filter(filename => filename !== "index.js")
        .map(async filename => bot.loadPlugin((await import("./" + filename)).default));

    return Promise.all(modules);
}