import jsonEasyFilter from "json-easy-filter";

const JefNode = jsonEasyFilter.JefNode;

function removeArray(arr) {
    let what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

export default function (bot) {

    bot.teamsPGM = {};

    const teamToExclude = ["TabView", "pgm", "Participants", "FakeTNT"];

    bot._client.on("teams", (packet) => {
        new JefNode(packet).filter((node) => {
            if (node.has("team"))
                if (!teamToExclude.some(team => node.value.team.includes(team))) {
                    if (node.value.mode == 0)
                        bot.teamsPGM[node.value.team] = node.value.players;
                    else if (node.value.mode == 1)
                        delete bot.teamsPGM[node.value.team];
                    else if (node.value.mode == 3)
                        bot.teamsPGM[node.value.team].push(node.value.players[0]);
                    else if (node.value.mode == 4 && Object.keys(bot.teamsPGM).length > 0)
                        removeArray(bot.teamsPGM[node.value.team], node.value.players[0]);
                }
        });
    });
}