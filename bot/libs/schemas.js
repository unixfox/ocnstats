import dynamoose from "dynamoose";
const Schema = dynamoose.Schema;

export const player = new Schema({
    uuid: {
        type: String,
        index: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    rank: {
        type: Number,
        required: true
    },
    kills: {
        type: Number,
        default: 0
    },
    deaths: {
        type: Number,
        default: 0
    },
    online: {
        type: Boolean,
        default: false
    }
});

export const kill = new Schema({
    matchTime: {
        type: Number,
        required: true
    },
    victimUUID: {
        type: String,
        required: true
    },
    killerUUID: {
        type: String,
        required: false,
        default: ""
    }
});