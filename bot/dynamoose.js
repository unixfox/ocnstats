const dynamoose = require("dynamoose");
dynamoose.local();
var Schema = dynamoose.Schema;

var dogSchema = new Schema({
    awards: {
        type: "list",
        list: [{
            type: "map",
            map: {
                year: Number,
                name: String
            }
        }]
    }
},
    {
        throughput: { read: 15, write: 5 }
    });
const Dog = dynamoose.model("Dog", dogSchema);
const odie = new Dog({
    ownerId: 4,
    name: "Odie",
    breed: "Beagle",
    color: ["Tan"],
    cartoon: true
});
odie.save(function (err) {
    if (err) {
        return console.log(err);
    }

    console.log("Ta-da!");
});