import mineflayer from "mineflayer/index.js";
import tokensMineflayer from "prismarine-tokens/index.js";
import { promisify } from "util";
import botEvents from "./libs/events/index.js";
import loadMineflayerCustomPlugins from "./libs/plugins/index.js";

const promiseTokensMineflayer = promisify(tokensMineflayer.use);

const options = {
    host: process.env.MC_IP,
    port: process.env.MC_PORT,
    username: process.env.MC_USERNAME,
    password: process.env.MC_PASSWD,
    verbose: true,
    version: "1.12.2",
    tokensLocation: "./bot_tokens.json",
    tokensDebug: true
};

promiseTokensMineflayer(options).then(options => {
    if (options.session) {
        launchBot(options);
    }
    else {
        console.log("Something went wrong during the authentication. Waiting 2 minutes to avoid Mojang locking the account.");
        setTimeout(() => {
            throw("Something went wrong during the authentication.");
        }, 120000);
    }
});

function launchBot(options) {
    const bot = mineflayer.createBot(options);

    loadMineflayerCustomPlugins(bot);
    botEvents(bot);

    bot.on("kicked", (reason, loggedIn) => {
        if (loggedIn && reason.text)
            if (reason.text.includes("restart"))
                setTimeout(() => {
                    launchBot(options);
                }, 10000);
            else {
                console.log("Banned or internal error in the server.");
                setTimeout(() => {
                    launchBot(options);
                }, 30000);
            }
    });

    bot.on("error", (error) => {
        if (error.code == "ECONNRESET") {
            launchBot(options);
        }
        else if (error.code == "ECONNREFUSED" || error.code == "ETIMEDOUT") {
            setTimeout(() => {
                launchBot(options);
            }, 10000);
        }
        console.log(error.code);
    });
}